﻿using System.Collections.Generic;
using AppExample.Core.Components;
using AppExample.Core.Data.Controllers;
using AppExample.Core.Data.Entities;
using AppExample.Core.Model;
using AppExample.Core.Webservices;
using AutoMapper;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using FizzWare.NBuilder;
using Moq;
using NUnit.Framework;

namespace AppExample.Test
{
    [TestFixture]
    class TestWebServices
    {
        private FormController _controller;
        private Mock<IUserController> _mockUserController;
        private Mock<IPortalController> _mockPortalController;
        private PersonFormController _personController;
        private Mock<IPersonController> _mockPersonController;
        private Mock<IDownloadPersons> _mockPersons;

        [SetUp]
        public void Setup()
        {
            //Portal Mock
            _mockPortalController = new Mock<IPortalController>();
            PortalController.SetTestableInstance(_mockPortalController.Object);
            _mockPortalController.Setup(x => x.GetCurrentPortalSettings()).Returns(new PortalSettings { PortalId = 1 });

            //User Mock
            _mockUserController = new Mock<IUserController>();
            UserController.SetTestableInstance(_mockUserController.Object);
            _mockUserController.Setup(x => x.GetCurrentUserInfo()).Returns(new UserInfo { Email = ""});

            //Person Mock
            _mockPersonController = new Mock<IPersonController>();
            PersonController.SetTestableInstance(_mockPersonController.Object);
            IEnumerable<Person> persons = Builder<Person>.CreateListOfSize(10)
                .All()
                .TheLast(1)
                .With(x => x.Id = 123)
                .Build();
            _mockPersonController.Setup(x => x.Get()).Returns(persons);

            Mapper.CreateMap<Person, PersonModel>()
                .ForMember(x => x.FirstName, opt => opt.MapFrom(c => $"{c.FirstName} {c.LastName}"));

            _controller = new FormController(new FakeMailWrapper());

            _mockPersons = new Mock<IDownloadPersons>();
            _mockPersons.Setup(x => x.GetPersons()).ReturnsAsync(persons);

            _personController = new PersonFormController(_mockPersons.Object);
        }

        [Test]
        public void TestTestPost()
        {
            //Arrange
            //Act
            var result = _controller.TestPost(true);

            //Assert
            Assert.IsTrue(result);
        }

        [Test]
        public void TestSendEmail()
        {
            //Arrange
            FormController.SendEmailDto dto = new FormController.SendEmailDto
            {
                Name = "My name",
                Email = "email@email.com",
                Message = "Message Example"
            };
            string email = "x@x.com";
            _mockUserController.Setup(x => x.GetCurrentUserInfo()).Returns(new UserInfo { Email = email });

            //Act
            var result = _controller.SendEmail(dto);

            //Assert
            Assert.AreEqual(result, "¡Your message was sent to " + dto.Email + "!");
        }

        [Test]
        public void TestGetPersons()
        {
            //Arrange
            //Act
            List<PersonModel> persons = _personController.GetPersons();

            ////Assert
            Assert.GreaterOrEqual(persons.Count, 10);
        }

        [Test]
        public void TestGetPerson()
        {
            //Arrange
            //Act
            PersonModel result = _personController.GetPerson(123);

            //Assert
            Assert.AreEqual(123, result.Id);
        }

        [Test]
        public async void TestFilterPersons()
        {
            //Arrange
            //Act
            string test1 = "FirstName1";
            
            List<PersonModel> result = await _personController.FilterPersons(test1);

            //Assert
            foreach (PersonModel pm in result)
            {
                Assert.AreEqual(pm.FirstName.ToLower().Substring(0, test1.Length), test1.ToLower());
            }
        }

        public class FakeMailWrapper : IMailWrapper
        {
            public void SendEmail(string fromAddress, string toAddress, string subject, string body){}
        }

    }
}