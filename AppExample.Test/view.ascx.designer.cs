//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace AppExample.Test {
    
    
    public partial class view {
        
        /// <summary>
        /// angularJS control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DotNetNuke.Web.Client.ClientResourceManagement.DnnJsInclude angularJS;
        
        /// <summary>
        /// ctlAppInclude control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DotNetNuke.Web.Client.ClientResourceManagement.DnnJsInclude ctlAppInclude;
        
        /// <summary>
        /// ctlAppstyleInclude control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::DotNetNuke.Web.Client.ClientResourceManagement.DnnCssInclude ctlAppstyleInclude;
        
        /// <summary>
        /// pnlMain control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Panel pnlMain;
        
        /// <summary>
        /// btnMain control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Button btnMain;
        
        /// <summary>
        /// dlExample control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList dlExample;
    }
}
