<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="view.ascx.cs" Inherits="AppExample.Test.view" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<dnn:DnnJsInclude ID="angularJS" runat="server" Priority="100" FilePath="//ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.js" />
<dnn:DnnJsInclude ID="ctlAppInclude" runat="server" Priority="101"  />
<dnn:DnnCssInclude ID="ctlAppstyleInclude" runat="server" Priority="102"  />

<base href="<%= DotNetNuke.Common.Globals.NavigateURL() %>/">
<asp:Panel id="pnlMain" runat="server" DefaultButton="btnMain">
    <div>
        <div class="splash container" data-ng-cloak>
            <h1 class="text-center">Loading. Please wait</h1>
        </div>
        <div data-ng-cloak>
            <div data-ng-include="'app/form/view/navigation.html'"></div>
            <data-form-view></data-form-view>
        </div>
        <asp:Button runat="server" ID="btnMain" style="display:none;" OnClientClick="return false;" />
    </div>
</asp:Panel>
<asp:DropDownList runat="server" ID="dlExample">
    <asp:ListItem Value="0">0</asp:ListItem>
    <asp:ListItem Value="0">1</asp:ListItem>
    <asp:ListItem Value="0">2</asp:ListItem>
    <asp:ListItem Value="0">3</asp:ListItem>
</asp:DropDownList>

<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">

<script type="text/javascript" src="//cdn.jsdelivr.net/restangular/1.4.0/restangular.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.15/angular-ui-router.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.7.0/underscore-min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.8/angular-animate.js"></script>

<script src="<%= ControlPath %>lib/angular-busy/dist/angular-busy.js"></script>
<link rel="stylesheet" href="<%= ControlPath %>lib/angular-busy/dist/angular-busy.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/angular.js/1.4.8/angular-sanitize.js"></script>
<script src="<%= ControlPath %>lib/ng-notify/src/scripts/ng-notify.js"></script>
<link rel="stylesheet" href="<%= ControlPath %>lib/ng-notify/src/styles/ng-notify.css">
<script src="<%= ControlPath %>lib/angular-messages/angular-messages.js"></script>

<script>
    (function ($) {
        $('form').attr('novalidate', 'novalidate');
        angular.module('AppExample.Test')
        .constant("_", _)
        .constant('dnnSF', $.ServicesFramework(<%= ModuleId %>))
            .config(['$locationProvider', function ($locationProvider) {
                    $locationProvider.html5Mode(true).hashPrefix('!'); 
                }
            ])
        .run(['$rootScope', function ($rootScope) {
            var onEnter = function () {
                $rootScope.$broadcast('enterClicked', '');
                $rootScope.$apply(function () {
                    $rootScope.submitted = true;
                });
            };
            angular.element(document.getElementById('<%= btnMain.ClientID %>')).click(onEnter);
        }]);
        angular.element(document).ready(function () {                
            angular.bootstrap(document.getElementById('<%= pnlMain.ClientID %>'), ['AppExample.Test']);
        });
    })($);
</script>