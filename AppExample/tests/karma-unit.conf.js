﻿module.exports = function (config) {
    config.set({
        files: [//Files we will make aur tests
        'bower_components/angular/angular.js',//Library
        'lib/jquery/dist/jquery.js',//library
        'lib/angular-ui-router/angular-ui-router.js',//library
        'lib/angular-mocks/angular-mocks.js',//library
        'lib/**/*.js',//library
        'app/**/*.js',//our appplication
        'tests/unit/**/*.js'//our tests
        ],
        basePath: '../',
        frameworks: ['jasmine'],//Testing Framework that we will work. It can be QUnit or other test framework.
        reporters: ['progress'],
        browsers: ['Chrome'],
        autoWatch: false,//When detecting a change in a file, karma going to make tests.
        singleRun: true,//After run test, it ends the process
        colors: true
        //plugins: ['plugin1', 'plugin2'] //plugins that we need.
    });
};