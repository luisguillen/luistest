/// <reference path="../../node_modules/karma-jasmine/lib/jasmine.js" />

describe("ContactForm", function () {

    describe("MainController", function() {
        var deferred, q, controller, scope;

        var mockServiceMetods = {
            customGET: function() {},
            customPOST: function() {}
        };
        var mockModuleServices = {
            getService: function() {
                return mockServiceMetods;
            }
        };
        beforeEach(function () {
            angular.module('AppExample-templates', []);

            module('AppExample.form', function ($provide) {
                $provide.value('moduleServices', mockModuleServices);
            });
        });

        var controllerFactory = inject(function ($controller, $q, $rootScope) {//?????
            q = $q;
            deferred = q.defer();
            scope = $rootScope.$new();
            controller = $controller("MainController", {$scope: scope});

        });


        describe('initialization', function () {
            beforeEach(function () {
                spyOn(mockModuleServices, 'getService').and.callThrough();
                controllerFactory();
            });

            it('should have databound test value', function () {
                expect(controller.test).toBeDefined();
            });

            it('should have datamodel', function () {
                expect(controller.model).toBeDefined();
            });

            it('it should initialize contact form service', function () {
                expect(mockModuleServices.getService).toHaveBeenCalledWith('contactForm','form');
            });
        });

        describe('tableactions', function () {
            beforeEach(function() {
                controllerFactory();
                for (var i = 0; i < 100; i++) {
                    controller.model.persons[i] = {Id: i, FirstName: "FirstName" + i, Age: _.random(15, 51)};
                }
            });

            it('It check if back function changed correctly from variable an to variable', function () {
                controller.model.from = 30;
                controller.model.to = 40;
                controller.model.length = 10;

                controller.model.back();
                expect(controller.model.from).toEqual(20);
                expect(controller.model.to).toEqual(30);

                controller.model.from = 0;
                controller.model.to = 10;
                controller.model.length = 10;

                controller.model.back();
                expect(controller.model.from).toEqual(0);
                expect(controller.model.to).toEqual(10);
            });

            iit('It check if next function changed correctly from variable an to variable', function () {
                controller.model.from = 30;
                controller.model.to = 40;
                controller.model.length = 10;

                controller.model.next();
                expect(controller.model.from).toEqual(40);
                expect(controller.model.to).toEqual(50);

                controller.model.from = 90;
                controller.model.to = 100;
                controller.model.length = 10;

                controller.model.next();
                expect(controller.model.from).toEqual(0);
                expect(controller.model.to).toEqual(10);
            });
        });

        describe('fomcontrols', function () {

            describe('Say hello GET', function () {
                beforeEach(function () {
                    spyOn(mockServiceMetods, 'customGET').and.callFake(function () { return deferred.promise; });
                    spyOn(mockServiceMetods, 'customPOST').and.callFake(function () { return deferred.promise; });
                    controllerFactory();
                });

                it('triggers validation messages', function () { 

                    controller.sayHello(false);

                    expect(controller.submitted).toEqual(true);
                });

                it('Calls hello world API method', function () {
                    controller.sayHello(true);
                    deferred.resolve(true);
                    scope.$apply();

                    expect(mockServiceMetods.customGET).toHaveBeenCalledWith('HelloWorld', { Name: controller.model.fullname() });
                    expect(controller.sent).toEqual(true);
                });

                it('Calls test post API method', function () {
                    controller.postTest(true);
                    deferred.resolve(true);
                    scope.$apply();

                    expect(mockServiceMetods.customPOST).toHaveBeenCalledWith({ Name: controller.model.fullname() }, 'TestPost');
                    expect(controller.sent).toEqual(true);
                });

                it('concatenates first and last name into full name', function () {
                    controller.model.firstName = 'Joe';
                    controller.model.lastName = 'Schmoe';
 
                    expect(controller.model.fullname()).toEqual(controller.model.firstName + ' ' + controller.model.lastName);
                });

                it('Call send an email API method', function() {
                    controller.sendEmail(true);
                    deferred.resolve(true); //??????
                    scope.$apply();

                    expect(mockServiceMetods.customPOST).toHaveBeenCalledWith({ Name: controller.model.fullname(), Email: controller.model.mail, Message: controller.model.message }, 'SendEmail');
                    expect(controller.sent).toEqual(true);
                });

                it('Call get persons API method', function () {
                    controller.getPersons(true);
                    deferred.resolve(true); //??????
                    scope.$apply();

                    expect(mockServiceMetods.customGET).toHaveBeenCalledWith('getPersons');
                    expect(controller.model.persons).toBeDefined();
                });

                it('Call get person (just one) API method', function () {
                    controller.getPerson(true);
                    deferred.resolve(true); //??????
                    scope.$apply();

                    expect(mockServiceMetods.customGET).toHaveBeenCalledWith('GetPerson', { Id: controller.model.getperson3 });
                    expect(controller.sent).toEqual(true);
                });
            });
        });
    });
});
