﻿    angular.module('md.messages')
        .factory('messages', messages);

    function messages(ngNotify) {
        ngNotify.config({
            position: 'top',
            html: true
        });


        return {
            showSuccessMessage: showSuccessMessage,
            showInfoMessage: showInfoMessage,
            showErrorMessage: showErrorMessage,
            dismiss: dismiss,
            showMessage: showMessage
        };

        function showMessage(message, type, autoHide) {
            ngNotify.config({
                sticky: !autoHide
            });
            ngNotify.set(message, type);
        }

        function showSuccessMessage(message, autoHide) {
            showMessage(message, 'success', autoHide);
        }

        function showInfoMessage(message, autoHide) {
            showMessage(message, 'info', autoHide);
        }

        function showErrorMessage(message, autoHide) {
            showMessage(message, 'error', autoHide);
        }

        function dismiss() {
            ngNotify.dismiss();
        }


    }