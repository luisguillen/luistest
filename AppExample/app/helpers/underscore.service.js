﻿    angular.module("underscore")
        .factory("_", underscore);

    function underscore() {
        return window._;
    }
