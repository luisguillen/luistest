﻿    angular.module('md.messages')
        .factory('errors', errors);

    function errors($rootScope, messages) {

        return {
            catch: catchError,
            showError: showError
        };

        function catchError(message, autoDelete, onError) {
            return function(reason) {
                if (reason.status !== 0) {
                    messages.showErrorMessage(message + " " + reason.data.Message, autoDelete || false);
                }
                $rootScope.loading = false;

                if (angular.isDefined(onError)) {
                    onError();
                }
            };
        }

        function showError(message) {
            messages.showErrorMessage(message);
            $rootScope.loading = false;
        }
    }