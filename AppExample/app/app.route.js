angular.module('AppExample.routing', ['ui.router'])
    .config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/");

    $stateProvider
        .state('home', {
            url: '/',
            template: '<div>Home' +
                '<div ui-view></div>'+
                '</div>'
        })
        .state('state1', {
            url: '/state1/',
            template: '<div><a ui-sref="state1.sub1">substate 1</a> <a ui-sref="state1.sub2">substate 2</a>' +
                '<div ui-view></div>' +
                '</div>'
        })
        .state('state1.sub1', {
            url: 'sub1/',
            template: '<div>Sub-state 1</div>'
        })
        .state('state1.sub2', {
            url: 'sub2/',
            template: '<div>Sub-state 2</div>'
        })
        .state('state2', {
            url: '/state2/',
            template: '<div>State 2</div>'
        });
});