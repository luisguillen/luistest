﻿/// <reference path="../../references.js" />

app.dataModule
        .factory('postItemSrv', postItemSrv);

    function postItemSrv(moduleServices, errors) {
        var webservices = moduleServices.getService('contactForm', 'form');

        return {
            postItemForm: postItemForm,
            helloWorld: helloWorld,
            sendEmail: sendEmail
        };

        function helloWorld(model, onSuccess, onError) {
            return webservices.customGET('HelloWorld', { Name: model })
                .then(onSuccess)
                .catch(errors.catch('Error post form.', false, onError));
        }

        function postItemForm(model, onSuccess, onError) {
            return webservices.customPOST(model, 'TestPost')
                .then(onSuccess)
                .catch(errors.catch('Error post form.', false, onError));
        }

        function sendEmail(model, onSuccess, onError) {
            return webservices.customPOST(model, 'SendEmail')
                .then(onSuccess)
                .catch(errors.catch('Error post form.', false, onError));
        }

    }