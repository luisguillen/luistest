﻿/// <reference path="../../references.js" />

app.dataModule
        .factory('personSrv', personSrv);

function personSrv(moduleServices, errors) {
    var webservices = moduleServices.getService('personForm', 'PersonForm');

    return {
        getPersons: getPersons,
        getPerson: getPerson,
        filterPerson: filterPerson
    };

    function getPersons(onSuccess, onError) {
        return webservices.customGET('getPersons')
            .then(onSuccess)
            .catch(errors.catch('Error post form.', false, onError));
    }

    function getPerson(model, onSuccess, onError) {
        return webservices.customGET('GetPerson', { Id: model })
            .then(onSuccess)
            .catch(errors.catch('Error post form.', false, onError));
    }

    function filterPerson(model, onSuccess, onError) {
        return webservices.customGET('filterPersons', { filterText: model })
            .then(onSuccess)
            .catch(errors.catch('Error post form.', false, onError));
    }
}