﻿angular.module('md.DNNModuleServices')
    .factory('dnnModuleServicesRestangular',dnnModuleServicesRestangular);


    function dnnModuleServicesRestangular(dnnSF, Restangular) {
        return Restangular.withConfig(function(RestangularConfigurer) {

            var headers = {};
            var tabId = dnnSF.getTabId();
            var afValue = dnnSF.getAntiForgeryValue();

            if (tabId > -1) {
                headers.ModuleID = dnnSF.getModuleId();
                headers.TabId = tabId;
            }

            
            if (afValue) {
                headers.RequestVerificationToken = afValue;

            }
            RestangularConfigurer.setDefaultHeaders(headers);
        });
    }