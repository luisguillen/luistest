﻿angular.module('md.DNNModuleServices')
    .factory('moduleServices',moduleServices);

// moduleServices is a service used to set the base url based on service to be called

function moduleServices(dnnModuleServicesRestangular) {
    return {
        getService: function(namespace, controller) {
            dnnModuleServicesRestangular.setBaseUrl('/desktopmodules/' + namespace + '/API/');
            if (controller !== undefined) {
                return dnnModuleServicesRestangular.all(controller);
            }
            return dnnModuleServicesRestangular;
        }
    };
}