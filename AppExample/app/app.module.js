/// <reference path="../references.js" />

var app = angular.module('AppExample', [
    'AppExample-templates',
    'md.DNNModuleServices',
    'ui.router',
    'AppExample.routing',
    'AppExample.form',
    'debounce'
]);