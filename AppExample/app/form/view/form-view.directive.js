/// <reference path="../../references.js" />

app.formViewModule
    .directive('formView', function() {
    	return {
    		restrict: 'AE',
    		replace: true,
    		templateUrl: 'app/form/view/formview.html',
    		controller: 'MainController',
    		controllerAs: 'ctrl',
    		bindToController: true
    	};
    });