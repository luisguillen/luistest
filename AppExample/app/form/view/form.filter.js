﻿app.formViewModule
   .filter('searchPerson', personFilter);

app.formViewModule
   .filter('justNPersons', justNPersons);

function personFilter(_) {

    return function (input, string) {
        if (string.length > 2) {

            return _.filter(input, function(person) {
                return person.FirstName.startsWith(string);
            });

        }else
            return input;
    };
}

function justNPersons() {

    return function(input, from, to) {

        return input.slice(from, to);

    };
}