/// <reference path="../../references.js" />

app.formViewModule
   .controller('MainController',MainController);

    
function MainController(moduleServices, _, postItemSrv, personSrv) {
    
	/* jshint validthis:true */
	var self = this;
	self.test = 'I am databound!!';
    
    self.sayHello = sayHello;
    self.postTest = postTest;
    self.sendEmail = sendEmail;
    self.getPersons = getPersons;
    self.getPerson = getPerson;
    self.getFilteredPerson = getFilteredPerson;

    self.model = {
        firstName: 'Joe',
        lastName: 'Doe',
        fullname:
            function() {
                return this.firstName + ' ' + this.lastName;
            },
        postItemPromise: null,
        mail: 'example@email.com', //This line change
        message: 'message',
        persons: [],
        personsInput: [],
        getperson3: 0,
        getSelectedPerson:
            function() {
                this.getperson3 = $('#getperson2').val();
            },

        //Table functionality.
        search: '',
        length: 10,
        from: 0,
        to: 10,
        back:
            function() {
                if (this.from - this.length >= 0) {
                    this.from -= this.length;
                    this.to -= this.length;
                } else {
                    this.setLength();
                }
            },
        next:
            function() {
                if (this.from + this.length < this.persons.length) {
                    this.from += this.length;
                    this.to += this.length;
                } else {
                    this.setLength();
                }
            },
        setLength:
            function() {
                this.from = 0;
                this.to = this.length;
            }
};


    function sayHello(isValid) {
    	self.submitted = true;

        if (isValid) {
            self.loading = true;

            self.model.postItemPromise = postItemSrv.helloWorld(self.model.fullname(), function (data) {
                if (data !== undefined && data !== null) {
                    self.test = data;
                    self.sent = true;
                }

            });

        }
    }

    function postTest(isValid) {
        self.submitted = true;
        if (isValid) {
            self.loading = true;
            
            self.model.postItemPromise = postItemSrv.postItemForm({ Name: self.model.fullname() }, function(data) {
                if (data !== undefined && data !== null) {
                    
                    self.sent = true;
                }

            });
  
        }
    }

    function sendEmail(isValid) {
        self.submitted = true;

        if (isValid) {
            self.loading = true;

            self.model.postItemPromise = postItemSrv.sendEmail({ Name: self.model.fullname(), Email: self.model.mail, Message: self.model.message }, function (data) {
                if (data !== undefined && data !== null) {
                    self.sent = true;
                    self.test = data;
                }
            });
        }
    }

    function getPersons(isValid) {
        self.submitted = true;

        if (isValid) {

            self.model.postItemPromise = personSrv.getPersons(function (data) {
                
                if (data !== undefined && data !== null) {
                    self.model.personsInput = data;
                }
                
            });

        }
    }

    function getPerson(isValid) {
        self.submitted = true;

        if (isValid) {
            self.loading = true;

            self.model.postItemPromise = personSrv.getPerson(self.model.getperson3, function (data) {

                if (data !== undefined && data !== null) {
                    self.sent = true;
                    self.test = "Hi " + data.FirstName + " your Id is " + data.Id;
                }

            });
        }

    }

    function getFilteredPerson(isValid) {
        self.submitted = true;

        if (isValid && self.model.search.length > 2) {

            self.model.postItemPromise = personSrv.filterPerson(self.model.search, function (data) {

                if (data !== undefined && data !== null) {
                    self.model.persons = data;
                }

            });
        }

    }

}
 