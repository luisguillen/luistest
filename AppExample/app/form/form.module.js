/// <reference path="../references.js" />

app.formViewModule = angular.module('AppExample.form', [
        'AppExample-templates',
        'md.DNNModuleServices',
        'md.data',
        'underscore',
        //'_',
        'cgBusy',
        'md.messages'
]);