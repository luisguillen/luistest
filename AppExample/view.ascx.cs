using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Framework;
using DotNetNuke.Security;
using DotNetNuke.Web.Api;

namespace AppExample
{
    public partial class view : PortalModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ServicesFramework.Instance.RequestAjaxAntiForgerySupport();
            ctlAppInclude.FilePath = ControlPath + "dist/app.js";
            ctlAppstyleInclude.FilePath = ControlPath + "dist/app.css";
        }
    }
}