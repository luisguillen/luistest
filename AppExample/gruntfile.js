"use strict";

var path = require('path');
module.exports = function (grunt) {
    grunt.initConfig({//Initial Configuration. Contains configuration from plugins.
        pkg: grunt.file.readJSON('package.json'),
           ngAnnotate: {
            options: {
                //singleQuotes: true
            },
            app: {
                files: [
                    {
                        //expand: true,
                        ext: 'annotated.js',
                        extDot: 'last',
                        'annotated/app.js': ['dist_temp/app/*.js','dist_temp/app/**/*.js']
                    }]
            }
        },
        bower: {
            dev: {
                dest: 'lib',
                options: {
                    expand: true,
                    packageSpecific: {
                        'angular': {
                            files: [
                              "angular.min.js"
                            ]
                        }
                    }
                }
            }
        },
        jshint: {
            options: {
                esnext:true
            },
            all: [ 'app/*.js','app/**/*.js']
        },
        html2js: {
            options: {
                base: path.resolve(),
                module: 'AppExample-templates'
            },
            dev: {
                src: ['app/**/*.html','app/*.html'],
                dest: 'tmp/templates.js'
            },
            dist: {
                src: ['app/**/*.html'],
                dest: 'tmp/templates.js'
            }
        },
        concat: {
            js: {
                   options: {
                        separator: ';\n',
                        banner: "(function(){\n'use strict';\n",
                        footer: '\n})();',
                        process: function (src, filepath) {
                            return '// Source: ' + filepath + '\n' +
                              src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
                        },
                    },
                src: ['annotated/*.js', 'tmp/*.js'],
                dest: 'dist/app.js'
            },
            css: {
                src: ['app/*.css', 'app/**/*.css', 'tmp/*.css'],
                dest: 'dist/app.css'
            }
        },
        uglify: {
            dist: {
                files: {
                    'dist/app.js': ['dist/app.js']
                },
                options: {
                    mangle: true
                }
            }
        },
        cssmin: {
            dist: {
                src: 'dist/app.css',
                dest: 'dist/app.css'
            }
        },
        clean: {
            temp: {
                src: ['tmp']
            },
            install: ['resources.zip','tmp/','annotated/','dist_temp/*']
        },
        watch: {
            dev: {
                files: ['gruntfile.js', 'app/*.js', 'app/*.html', 'app/**/*.html', 'app/*.ascx', 'app/*.less', 'app/**/*.js', 'app/**/*.less'],
                tasks: ['babel', 'ngAnnotate:app', 'html2js:dev','less:development','copy:main', 'concat:js', 'concat:css', 'jshint','removelogging','comments','clean:install'],
                options: {
                    atBegin: true
                }
            },
            min: {
                files: ['gruntfile.js', 'app/*.js', '*.html'],
                tasks: ['jshint', 'html2js', 'concat:dist', 'clean:temp'],
                options: {
                    atBegin: true
                }
            }
        },
        compress: {
            main: {
                options: { archive: "install/install.zip" },
                files: [
                    {src: '*.dnn',dest:''},
                    {src: 'bin/AppExample.dll',dest:''},
                    {src: 'bin/AppExample.Core.dll',dest:''},
                    {src: 'resources.zip',dest:''}
                ]
            },
            resources: {
                options: { archive: "resources.zip" },
                files: [
                    { src: '*.ascx', dest: '' },
                    { src: 'dist/**', dest: '' },
                    { src: 'app_Localresources/**', dest: '' },
                    { src: 'lib/**', dest: '' }
                ]
            }
        },
        karma: {
            unit: {
                configFile: './tests/karma-unit.conf.js',
                autoWatch: false,
                singleRun: true
            },
            unit_auto: {
                configFile: './tests/karma-unit.conf.js',
                autoWatch: true,
                singleRun: false
            },
            unit_coverage: {
                configFile: './tests/karma-unit.conf.js',
                autoWatch: false,
                singleRun: true,
                reporters: ['progress', 'coverage'],
                preprocessors: {
                    'app/scripts/*.js': ['coverage']
                },
                coverageReporter: {
                    type: 'html',
                    dir: 'coverage/'
                }
            },
        },
        less: {
            development: {
                options: {
                    compress: false
                },
                files: {
                    "app/app.css": "app/styles/app.less"
                }
            },

            //topNav: {
            //    options: {
            //        compress: false
            //    },
            //    files: {
            //        "bootstrapNav/topNav.css": "styles/navigation/topNav.less"
            //    }
            //}


        },
        copy: {
            main: {
                files: [
                  { expand: true, src: ['app/styles/fonts/*'], dest: 'dist/fonts', filter: 'isFile', flatten: true }
                ],
            },
        },
         removelogging: {
            dist: {
                src: "dist/app.js",
                dest: "dist/app.js",
 
            }
        },
         comments: {
             live: {
                 // Target-specific file lists and/or options go here. 
                 options: {
                     singleline: true,
                     multiline: true
                 },
                 src: ['dist/*.js'] // files to remove comments from 
             }
         },
        babel: {
             options: {
                 sourceMap: false,
                 presets: ['es2015']
             },
             dist: {
                 files: [{
                     expand: true,
                     src: ['app/**/*.js'],
                     dest: 'dist_temp/'
                 } ]
             }
         }
    });//End Configuration
    
    grunt.loadNpmTasks('grunt-ng-annotate');//Load a plugin. In this case load grunt-ng-annotate plugin
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-connect');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');//Minify the js
    grunt.loadNpmTasks('grunt-html2js');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-bower');
    grunt.loadNpmTasks('grunt-karma');
    grunt.loadNpmTasks('grunt-css');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks("grunt-remove-logging");
    grunt.loadNpmTasks('grunt-stripcomments');
    grunt.loadNpmTasks('grunt-babel');

    grunt.registerTask('dev', ['bower','watch:dev']);//register a task (frst parameter) with their tasks names (second parameter).
    grunt.registerTask('live', ['babel','ngAnnotate:app', 'html2js:dev', 'less:development','concat:js', 'concat:css', 'uglify:dist','cssmin:dist', 'compress:resources', 'compress', 'clean:install','removelogging','comments']);
    //grunt.registerTask('pub', ['html2js', 'concat:dist', 'uglify:dist', 'clean:temp']);
    grunt.registerTask('autotest', ['karma:unit_auto']);
    grunt.registerTask('default', 'dev');
};