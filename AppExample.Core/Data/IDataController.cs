using System.Collections.Generic;
using AppExample.Core.Data.Entities;

namespace AppExample.Core.Data
{
    public interface IDataController<T> where T: IPerson
    {
        IEnumerable<T> Get();
        T GetById(int id);
        void Add(T item);
        void Update(T item);
        void Delete(T item);
        void Delete(int id);
		void AddRange(IEnumerable<T> items);
        void DeleteRange(IEnumerable<T> items);
    }
}