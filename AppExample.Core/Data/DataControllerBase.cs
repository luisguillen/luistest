using System.Collections.Generic;
using AppExample.Core.Data.Entities;
using System.Linq;

namespace AppExample.Core.Data
{
    public abstract class DataControllerBase<T> : IDataController<T> where T : IPerson
    {
        protected readonly ISimpleRepository<T> Repository;

        protected DataControllerBase(ISimpleRepository<T> repository )
        {
            Repository = repository;
        }

        virtual public IEnumerable<T> Get()
        {
            return Repository.Get() ?? new List<T>();
        }

        virtual public T GetById(int id)
        {
            return Repository.GetById(id);
        }

        virtual public void Add(T item)
        {
            Repository.Add(item);
        }

        virtual public void Update(T item)
        {
            Repository.Update(item);
        }

        virtual public void Delete(T item)
        {
            Repository.Delete(item);
        }

        virtual public void Delete(int id)
        {
            var item = Repository.GetById(id);
            Delete(item);
        }
		
		virtual public void AddRange(IEnumerable<T> items)
        {
            items.ToList().ForEach(Add);
        }

        virtual public void DeleteRange(IEnumerable<T> items)
        {
            items.ToList().ForEach(Delete);
        }
    }
}