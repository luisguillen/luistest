using System;
using System.Linq;
using AppExample.Core.Data.Entities;
using Faker;
using FizzWare.NBuilder;

namespace AppExample.Core.Data.Repositories
{
    public class FakePersonRepository : SimpleRepositoryBase<Person>
    {
        public FakePersonRepository()
        {
            All = Builder<Person>.CreateListOfSize(100)
                .All()
                .With(x => x.Age = new RandomGenerator().Next(15, 51))
                .With(x => x.FirstName = NameFaker.FirstName())
                .With(x => x.LastName = NameFaker.LastName())
                .With(x => x.Email = Faker.InternetFaker.Email())
                .Build().ToList();
        }
    }
}