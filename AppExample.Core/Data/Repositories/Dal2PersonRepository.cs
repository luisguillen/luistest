using System.Collections.Generic;
using System.Linq;
using AppExample.Core.Data.Entities;
using DotNetNuke.Data;

namespace AppExample.Core.Data.Repositories
{
    public class Dal2PersonRepository : SimpleRepositoryBase<Person>
    {
        public override IEnumerable<Person> Get()
        {
            var results = new List<Person>();
            using (var dataContext = DataContext.Instance())
            {
                var repository = dataContext.GetRepository<Person>();
                results = repository.Get().ToList();
            }
            return results;
        }

        public override Person GetById(int id)
        {
            Person item = null;
            using (var dataContext = DataContext.Instance())
            {
                var repository = dataContext.GetRepository<Person>();
                item = repository.GetById(id);
            }
            return item;
        }

        public override void Add(Person item)
        {
            using (var dataContext = DataContext.Instance())
            {
                var repository = dataContext.GetRepository<Person>();
                repository.Insert(item);
            }
        }

        public override void Update(Person item)
        {
            using (var dataContext = DataContext.Instance())
            {
                var repository = dataContext.GetRepository<Person>();
                repository.Update(item);
            }
        }

        public override void Delete(Person item)
        {
            using (var dataContext = DataContext.Instance())
            {
                var repository = dataContext.GetRepository<Person>();
                repository.Delete(item);
            }
        }

        public override void Delete(int id)
        {
            var item = GetById(id);
            Delete(item);
        }
    }
}