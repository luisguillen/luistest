using System.Linq;

namespace AppExample.Core.Data.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<T> Paging<T>(this IQueryable<T> query, int currentPage, int pageSize)
        {
            return query
                .Skip(currentPage * pageSize)
                .Take(pageSize);
        }
    }
}