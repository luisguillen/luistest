using System.Collections.Generic;
using System.Linq;
using AppExample.Core.Data.Entities;

namespace AppExample.Core.Data
{
    public abstract class SimpleRepositoryBase<T> : ISimpleRepository<T> where T : IPerson
    {

        private List<T> _all;

        public virtual List<T> All
        {
            get { return _all; }
            set { _all = value; }
        }

        public virtual IEnumerable<T> Get()
        {
            return _all;
        }

        public virtual void Add(T item)
        {
            var nextId = _all.Select(x => x.Id).Max() + 1;
            item.Id = nextId;
            _all.Add(item);
        }

        public virtual void Update(T item)
        {
            int index = _all.FindIndex(x => x.Id == item.Id);
            _all[index] = item;
        }

        public virtual void Delete(T item)
        {
            _all.Remove(item);
        }

        public virtual void Delete(int id)
        {
            var item = GetById(id);
            Delete(item);
        }

        public virtual T GetById(int id)
        {
            return Enumerable.SingleOrDefault<T>(_all, x => x.Id == id);
        }
		
		public virtual void AddRange(IEnumerable<T> items)
        {
            items.ToList().ForEach(Add);
        }

        public virtual void DeleteRange(IEnumerable<T> items)
        {
            items.ToList().ForEach(Delete);
        }
    }
}