using System.Collections.Generic;
using System.Linq;
using DotNetNuke.Data;
using DotNetNuke.Entities.Portals;
using AppExample.Core.Data.Entities;

namespace AppExample.Core.Data.Controllers
{
    public class SettingsControllerImpl : ISettingsController
    {


        public void UpdateSetting(string key, string value)
        {
            using (var dataContext = DataContext.Instance())
            {
                SettingInfo setting = GetSettingByName(key) ?? new SettingInfo {Key = key, PortalId = PortalSettings.Current.PortalId};
                setting.Value = value;
                if (setting.Id < 1)
                {
                    dataContext.GetRepository<SettingInfo>().Insert(setting);
                }
                else
                {
                    dataContext.GetRepository<SettingInfo>().Update(setting);
                }
            }
        }

        public string GetSetting(string settingName, string defaultValue)
        {
            SettingInfo setting = GetSettingByName(settingName);
            return (setting != null) ? setting.Value : defaultValue;
        }

        public IEnumerable<SettingInfo> GetSettings()
        {
            using (var dataContext = DataContext.Instance())
            {
                return dataContext.GetRepository<SettingInfo>().Get(PortalSettings.Current.PortalId);
            }
        }

        private SettingInfo GetSettingByName(string name)
        {
            return GetSettings().SingleOrDefault(a => a.Key == name);
        }
    }
}