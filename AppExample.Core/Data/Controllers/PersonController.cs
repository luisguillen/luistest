﻿using System;
using DotNetNuke.Framework;

namespace AppExample.Core.Data.Controllers
{
    public class PersonController : ServiceLocator<IPersonController, PersonController>
    {
        protected override Func<IPersonController> GetFactory()
        {
            return () => new PersonControllerImpl();
        }
    }
}