﻿using AppExample.Core.Data.Entities;

namespace AppExample.Core.Data.Controllers
{
    public interface IPersonController : IDataController<Person>
    {
    }
}