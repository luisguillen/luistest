using System;
using DotNetNuke.Framework;

namespace AppExample.Core.Data.Controllers
{
    public class SettingsController : ServiceLocator<ISettingsController, SettingsController>
    {
        protected override Func<ISettingsController> GetFactory()
        {
            return () => new SettingsControllerImpl();
        }
    }
}