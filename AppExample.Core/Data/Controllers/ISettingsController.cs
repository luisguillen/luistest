using System.Collections.Generic;
using AppExample.Core.Data.Entities;

namespace AppExample.Core.Data.Controllers
{
    public interface ISettingsController
    {
        void UpdateSetting(string key, string value);
        string GetSetting(string settingName, string defaultValue);
        IEnumerable<SettingInfo> GetSettings();
    }
}