using AppExample.Core.Components;
using AppExample.Core.Data.Entities;
using AppExample.Core.Data.Repositories;

namespace AppExample.Core.Data.Controllers
{
    public class PersonControllerImpl : DataControllerBase<Person>, IPersonController
    {
        public PersonControllerImpl(ISimpleRepository<Person> repository) : base(repository)
        {
        }
        public PersonControllerImpl()
            : base(Util.GetRepository<Person, FakePersonRepository, Dal2PersonRepository>("AppExample.Core.Data.Controllers.PersonRepository"))
        {
        }
    }
}