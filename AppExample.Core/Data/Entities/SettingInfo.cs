using System.Web.Caching;
using DotNetNuke.ComponentModel.DataAnnotations;

namespace AppExample.Core.Data.Entities
{
    [TableName("AppExample.Core_Settings")]
    [PrimaryKey("ID", AutoIncrement = true)]
    [Scope("PortalId")]
    [Cacheable("AppExample.Core_Settings", CacheItemPriority.Normal, 20)]
    public class SettingInfo
    {
        public int Id { get; set; }
        public int PortalId { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}