
using AppExample.Core.Data;
using AppExample.Core.Data.Entities;
using DotNetNuke.ComponentModel;

namespace AppExample.Core.Components
{
    public static class Util
    {
        public static ISimpleRepository<TObjectType> GetRepository<TObjectType, TFakeRepositoryType, TRepositoryType>(string name)
            where TObjectType : IPerson
            where TRepositoryType : SimpleRepositoryBase<TObjectType>
            where TFakeRepositoryType : SimpleRepositoryBase<TObjectType>
        {
            bool useFakes = true;

            var repository = ComponentFactory.GetComponent(name) as ISimpleRepository<TObjectType>;

            if (repository != null) return repository;
            if (useFakes)
            {
                ComponentFactory.Container.RegisterComponent<ISimpleRepository<TObjectType>, TFakeRepositoryType>(name, ComponentLifeStyleType.Singleton);
            }
            else
            {
                ComponentFactory.Container.RegisterComponent<ISimpleRepository<TObjectType>, TRepositoryType>(name, ComponentLifeStyleType.Singleton);
            }

            repository = ComponentFactory.GetComponent(name) as ISimpleRepository<TObjectType>;

            return repository;
        }
    }

}