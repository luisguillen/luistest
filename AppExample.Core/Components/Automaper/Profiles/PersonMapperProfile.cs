﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AppExample.Core.Data.Entities;
using AppExample.Core.Model;
using AutoMapper;
using Faker;

namespace AppExample.Core.Components.Automaper.Profiles
{
    public class PersonMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Person, PersonModel>()
                .ForMember(x => x.FirstName, opt => opt.MapFrom(c => $"{c.FirstName} {c.LastName}"));
        }
    }
}
