using System;
using System.Collections;
using System.Collections.Generic;
using AppExample.Core.Components.Services;
using AutoMapper;

namespace AppExample.Core.Components.Extensions
{
    public static class AutoMapperExtensions
    {
        public static List<TResult> MapTo<TResult>(this IEnumerable self) where TResult: class
        {
            if (self == null)
                throw new ArgumentNullException();

            return AutoMapperService.Instance.Map<List<TResult>>(self);
        }

        public static TResult MapTo<TResult>(this object self) where TResult : class
        {
            if (self == null)
                throw new ArgumentNullException();

            return AutoMapperService.Instance.Map<TResult>(self);
        }

        public static TResult MapPropertiesToInstance<TResult>(this object self, TResult value)
        {
            if (self == null)
                throw new ArgumentNullException();

            return (TResult)Mapper.Map(self, value, self.GetType(), typeof(TResult));
        }

    }
}