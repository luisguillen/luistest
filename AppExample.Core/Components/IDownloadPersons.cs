﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AppExample.Core.Data.Entities;

namespace AppExample.Core.Components
{
    public interface IDownloadPersons
    {
        Task<IEnumerable<Person>> GetPersons();
        bool IsRunning { get; set; }
    }
}
