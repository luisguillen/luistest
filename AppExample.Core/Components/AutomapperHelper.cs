using AutoMapper;
using System;
using System.Linq;
using AppExample.Core.Components.Automaper.Profiles;
using AppExample.Core.Data.Entities;
using AppExample.Core.Model;


namespace AppExample.Core.Components
{
    public static class AutomapperHelper
    {
        public static void CreateMaps()
        {
           //Create your maps here. For example:
            //Mapper.CreateMap<Person, PersonModel>()
            //    .ForMember(x => x.FirstName, opt => opt.MapFrom(c => $"{c.FirstName} {c.LastName}"));
            //Or use profiles
            Mapper.AddProfile(new PersonMapperProfile());
        }
    }

}