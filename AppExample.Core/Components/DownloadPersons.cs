﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using AppExample.Core.Data.Controllers;
using AppExample.Core.Data.Entities;
using AppExample.Core.Wrappers;

namespace AppExample.Core.Components
{
    public class DownloadPersons : IDownloadPersons
    {
        public bool IsRunning { get; set; }

        public async Task<IEnumerable<Person>> GetPersons()
        {
            IsRunning = true;

            HttpClientWrapper.Instance.Init();
            HttpClientWrapper.Instance.AddUri("http://dnn742.dnndev.me/");
            HttpClientWrapper.Instance.AddMimeType("application/json");
            HttpResponseMessage response = await HttpClientWrapper.Instance.GetResponse("desktopmodules/personForm/API/PersonForm/getPersons");

            IEnumerable<Person> persons = new List<Person>();

            if(response.IsSuccessStatusCode)
                persons = await response.Content.ReadAsAsync<IEnumerable<Person>>();

            IsRunning = false;
            return persons;
        }

    }
}
