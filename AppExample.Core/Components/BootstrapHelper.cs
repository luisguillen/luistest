using DotNetNuke.Web.Api;

namespace AppExample.Core.Components
{
    public class BootstrapHelper : IServiceRouteMapper 
    {
        public void RegisterRoutes(IMapRoute mapRouteManager)
        {
                AutomapperHelper.CreateMaps();
        }
    }

}