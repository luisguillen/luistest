using System;
using DotNetNuke.Framework;

namespace AppExample.Core.Components.Services
{
    public class AutoMapperService : ServiceLocator<IAutoMapperService, AutoMapperService>
    {
        protected override Func<IAutoMapperService> GetFactory()
        {
            return () => new AutoMapperServiceImpl();
        }
    }
}