using System.Collections.Generic;

namespace AppExample.Core.Components.Services
{
    public interface IAutoMapperService
    {
        TDest Map<TSrc, TDest>(TSrc source) where TDest : class;
        TDest Map<TDest>(object source) where TDest : class;
        List<TDest> Map<TDest>(List<object> source) where TDest : class;
        TDest Map<TDest>(object source, TDest values) where TDest : class;
    }
}