using System.Collections.Generic;
using AutoMapper;

namespace AppExample.Core.Components.Services
{
    public class AutoMapperServiceImpl : IAutoMapperService
    {
       private readonly IMappingEngine _mapper;

        public AutoMapperServiceImpl():this(Mapper.Engine)
        {
        }

        public AutoMapperServiceImpl(IMappingEngine mapper)
        {
            _mapper = mapper;
        }

        public TDest Map<TSrc, TDest>(TSrc source) where TDest : class
        {
            return _mapper.Map<TSrc, TDest>(source);
        }
        public TDest Map<TDest>(object source) where TDest : class
        {
            return (TDest)_mapper.Map(source, source.GetType(), typeof(TDest));
        }
        public List<TDest> Map<TDest>(List<object> source) where TDest : class
        {
            return (List<TDest>)_mapper.Map(source, source.GetType(), typeof(List<TDest>));
        }

        public TDest Map<TDest>(object source, TDest values) where TDest : class
        {
            return (TDest)_mapper.Map(source, values, source.GetType(), typeof(TDest));
        }
    }
}