using DotNetNuke.Web.Api;

namespace AppExample.Core.Webservices
{
    public class RouteMapper : IServiceRouteMapper
    {
        public void RegisterRoutes(IMapRoute mapRouteManager)
        {
            mapRouteManager.MapHttpRoute("contactForm", "default", "{controller}/{action}", new[] { "AppExample.Core.Webservices" });
            mapRouteManager.MapHttpRoute("personForm",  "default", "{controller}/{action}", new[] { "AppExample.Core.Webservices" });
        }
    }
}