using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Web.Http;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Users;
using DotNetNuke.Security;
using DotNetNuke.Security.Roles;
using DotNetNuke.Services.Mail;
using DotNetNuke.Web.Api;

namespace AppExample.Core.Webservices
{
    //[HttpPost]
    //[HttpGet]
    //[AllowAnonymous]
    //[RequireHost]
    //[ValidateAntiForgeryToken]
    //[DnnModuleAuthorize(AccessLevel = SecurityAccessLevel.View)]
    //[DnnModuleAuthorize(AccessLevel = SecurityAccessLevel.Edit)]
    
    [AllowAnonymous]
    public class FormController : DnnApiController
    {
        private readonly IMailWrapper _mailWrapper;

        public FormController(IMailWrapper mailWrapper)
        {
            _mailWrapper = mailWrapper;
        }

        public FormController():this(new MailWrapper()){}

        delegate string addText(string inp1, string inp2);

        [HttpGet]
        public dynamic HelloWorld(string name)
        {
            try
            {
                addText example = (x, y) => x + y;

                return example("Hello bla bla", name);
            }
            catch (Exception exc)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, exc);
            }
        }

        [HttpPost]
        public bool TestPost(dynamic data)
        {
            return true;
        }

        [HttpPost]
        [DnnModuleAuthorize(AccessLevel = SecurityAccessLevel.Edit)]
        public string SendEmail(SendEmailDto data)
        {
            var user = PortalController.Instance.GetCurrentPortalSettings().UserInfo;
            _mailWrapper.SendEmail(user.Email, data.Email, "Email Test Sent by " + data.Name, data.Message);
            return "�Your message was sent to " + data.Email + "!";
        } 

        public class SendEmailDto
        {
            public string Name { get; set; }
            public string Email { get; set; }
            public string Message { get; set; }
        }

        async Task<int> AccessTheWebAsync()
        {
            HttpClient http = new HttpClient();

            return (await http.GetStringAsync("http://msdn.microsoft.com")).Length;
        }
    }

    public interface IMailWrapper
    {
        void SendEmail(string fromAddress, string toAddress, string subject, string body);
    }

    public class MailWrapper : IMailWrapper
    {
        public void SendEmail(string fromAddress, string toAddress, string subject, string body)
        {
            Mail.SendEmail(fromAddress,toAddress, subject,body);
        }
    }
}