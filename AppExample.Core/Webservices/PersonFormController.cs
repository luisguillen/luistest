﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AppExample.Core.Components;
using AppExample.Core.Components.Extensions;
using AppExample.Core.Data.Controllers;
using AppExample.Core.Data.Entities;
using AppExample.Core.Model;
using DotNetNuke.Web.Api;

namespace AppExample.Core.Webservices
{
    [AllowAnonymous]
    public class PersonFormController : DnnApiController
    {

        private readonly IDownloadPersons _download;

        public PersonFormController(IDownloadPersons download)
        {
            _download = download;
        }

        public PersonFormController() : this(new DownloadPersons()) { }

        [HttpGet]
        public List<PersonModel> GetPersons()
        {
            var result = PersonController.Instance.Get();

            return result.MapTo<PersonModel>();
        }

        [HttpGet]
        public PersonModel GetPerson(int id)
        {
            var result = PersonController.Instance.Get().
                SingleOrDefault(x => x.Id == id);

            return result.MapTo<PersonModel>();
        }

        [HttpGet]
        public async Task<List<PersonModel>> FilterPersons(string filterText)
        {
            //IEnumerable<Person> result = PersonController.Instance.Get()
            //    .Where(person => person.FirstName.ToLower().StartsWith(filterText.ToLower()))
            //    .OrderBy(person => person.Age);

            IEnumerable<Person> persons = await _download.GetPersons();

            IEnumerable<Person> result =
                from person in persons
                where person.FirstName.ToLower().StartsWith(filterText.ToLower())
                orderby person.Age //ascending default
                select person;

            return result.MapTo<PersonModel>();
        }
    }
}