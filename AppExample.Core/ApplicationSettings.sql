﻿*/ 
Search and replace [CUSTOMER] in the script below with the correct name and then run the script to create the settings table
*/

IF NOT EXISTS (select * from sys.objects where Object_ID = Object_ID(N'{databaseOwner}{objectQualifier}[CUSTOMER]_Settings'))
BEGIN

	CREATE TABLE [{databaseOwner}{objectQualifier}[CUSTOMER]_Settings](
		[ID] [int] IDENTITY(1,1) NOT NULL,
		[PortalID] [int] NOT NULL,
		[Key] [varchar](250) NOT NULL,
		[Value] [text] NULL,
	 CONSTRAINT [PK_[CUSTOMER]_Settings] PRIMARY KEY CLUSTERED 
	(
		[ID] ASC
	)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

END

GO