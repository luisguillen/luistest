﻿using System.Net.Http;
using System.Threading.Tasks;

namespace AppExample.Core.Wrappers
{
    public interface IHttpClientWrapper
    {
        void Init();
        void AddUri(string uri);
        void AddMimeType(string mime);
        Task<HttpResponseMessage> GetResponse(string uri);
    }
}
