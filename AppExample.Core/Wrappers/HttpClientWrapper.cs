﻿using System;
using DotNetNuke.Framework;

namespace AppExample.Core.Wrappers
{
    public class HttpClientWrapper : ServiceLocator<IHttpClientWrapper, HttpClientWrapper>
    {
        protected override Func<IHttpClientWrapper> GetFactory()
        {
            return () => new HttpClientWrapperImpl();
        }
    }
}
