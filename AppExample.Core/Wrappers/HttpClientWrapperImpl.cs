﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace AppExample.Core.Wrappers
{
    public class HttpClientWrapperImpl : IHttpClientWrapper
    {
        private HttpClient _http;

        public void Init() 
        {
            _http = new HttpClient();
        }

        public void AddUri(string uri)
        {
            _http.BaseAddress = new Uri("http://dnn742.dnndev.me/");
        }

        public void AddMimeType(string mime)
        {
            _http.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(mime));
        }

        public async Task<HttpResponseMessage> GetResponse(string uri)
        {
            _http.DefaultRequestHeaders.Accept.Clear();
            await Task.Delay(2000);
            return await _http.GetAsync(uri);
        }
    }
}
